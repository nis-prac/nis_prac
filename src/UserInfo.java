/**
 * Stores basic user info data
 * @author Louis Buys
 * 20 March 2015
 */

public class UserInfo
{
		public String name;
		public String HName;
		public String AuthKey;
		
		public  UserInfo(String name, String hostname, String authkey) {
			
			this.name = name;
			HName = hostname;
			AuthKey = authkey;
			
		}
}