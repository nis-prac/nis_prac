import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
/**
 * AS for secure chat.
 * Provides authentication and key distribution for chat clients.
 * @author Craig Feldman
 * @author Louis Buys
 * 
 * 20 March 2015
 */

public class AuthServer {

	static final boolean DEBUG = true;
	
	static ArrayList<UserInfo> users = new ArrayList<UserInfo>();
	
	public static void main(String[] args) {
		System.out.println("Server Running...\n");			

		//populate userinfo list
		users.add(new UserInfo("Louis","localhost","0123456789abcdeL"));
		users.add(new UserInfo("Craig","localhost","0123456789abcdeC"));

		int portNumber = 444;

		try (
			ServerSocket serverSocket = new ServerSocket(portNumber);
		    Socket clientSocket = serverSocket.accept();
		    PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
			DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream());
		    BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		)
		{
			// Read the message from B [A,B,Na,Nb] and generate a session key
			// AS <- B: A, B, Na, Nb
			System.out.println("Request received.");			
			if (DEBUG)
				System.out.println("\n2: AS <- B: A, B, Na, Nb");
			
			String[] msg = in.readLine().split(",");
			String A = msg[0];
			String B = msg[1];
			String nonceA = msg[2];
			String nonceB = msg[3];
			
			System.out.println("Authorisation request received from " + B + "(B) to connect to " + A + "(A)");
			System.out.println("[Na: " + nonceA + " Nb: " + nonceB + "]");
			
			// Now we must send encrypted session keys back to B
			// AS -> B: Eka[Ks, Na, Nb] , Ekb[ks, Nb, Na]
			if (DEBUG)
				System.out.println("\n3: AS -> B: Eka[Ks, Na, Nb] , Ekb[ks, Nb, Na]");
			// Generate session key
			
			String keySession = Crypto.generateKey();
			//String keySession = "0123456789abcdeS";
						
			System.out.println("Session key created [" + keySession + "]");
			String keyA = "";
			String keyB = "";
			
			// Get A and B's private key that they share with the server
			for (UserInfo u : users) {
				if (u.name.equals(A))
					keyA = u.AuthKey;
				else if (u.name.equals(B))
					keyB = u.AuthKey;					
			}
			
			try {
				byte[] EkaKs = Crypto.encrypt(keySession.getBytes(), keyA);
				byte[] EkaNa = Crypto.encrypt(nonceA.getBytes(), keyA);
				byte[] EkaNb = Crypto.encrypt(nonceB.getBytes(), keyA);
				
				byte[] EkbKs = Crypto.encrypt(keySession.getBytes(), keyB);
				byte[] EkbNa = Crypto.encrypt(nonceA.getBytes(), keyB);
				byte[] EkbNb = Crypto.encrypt(nonceB.getBytes(), keyB);
				
				//System.out.println(EkbKs.length);
				//System.out.println( new String(Crypto.decrypt(EkbKs, "0123456789abcdeC")));
				//System.out.println("Sending length " + EkaKs.length);
				dOut.writeInt(EkaKs.length);
				dOut.write(EkaKs);
				dOut.writeInt(EkaNa.length);
				dOut.write(EkaNa);
				dOut.writeInt(EkaNb.length);
				dOut.write(EkaNb);

				
				dOut.writeInt(EkbKs.length);
				dOut.write(EkbKs);
				dOut.writeInt(EkbNa.length);
				dOut.write(EkbNa);
				dOut.writeInt(EkbNb.length);
				dOut.write(EkbNb);
				
				out.println("Authenticated");


				
				System.out.println("\nDone.");
			//	Thread.sleep(10000);

			} catch (Exception e) {
				System.err.println("AS encryption error");
				e.printStackTrace();
			} 
		
		  
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
	}

}




