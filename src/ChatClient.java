import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Scanner;
/**
 * Client side of chat application
 * @author Craig Feldman
 * @author Louis Buys
 * 
 * 20 March 2015
 *
 */

public class ChatClient {

	final static boolean DEBUG = true;

	public static String username = "";
	public static String sessionKey = "";
	public static String authKey = "";
	public static String clientName = "";
	public static String currentNonce = "";
	static ArrayList<UserInfo> users = new ArrayList<UserInfo>();

	public static Crypto cryptAuth;
	public static Crypto cryptChat;
	
	static String nonceA;
	static String nonceB;
	static String receivedNonceA;
	static String receivedNonceB;
	static String A;
	static String B;
	
	static Scanner keys = new Scanner(System.in);
	
	
	
	
	/**
	 * 555 = client to client
	 * 444 = auth server 
	 */
	public static void main(String[] args) {
		
		////////////////////////////////////////////////////////////////////////
		//populate userinfo list		
		users.add(new UserInfo("Louis","localhost","0123456789abcdeL"));
		users.add(new UserInfo("Craig","localhost","0123456789abcdeC"));
		////////////////////////////////////////////////////////////////////////

		String hostName = "localhost";


		System.out.println("\n===============================");
		System.out.println("     Welcome to Secure Chat    ");	
		System.out.println("===============================\n");
		
		System.out.println("Please enter your username:");
		System.out.print(">> ");
		
		username = keys.nextLine();
		
		System.out.println("\nEnter client username to connect to or l to wait for a connection:");
		
		String action = keys.nextLine();
		
		//lookup hostname and authkey of username entered
		
			// Look up the User and set hostname and auth key
		for (UserInfo u : users)
		{
			if (u.name.equals(username))
			{
				hostName = u.HName;
				authKey = u.AuthKey;
			}
		}
		

		if (action.equals("l"))
		{
			System.out.println("\nWaiting for connection...");
			///////////////////////////////////////////////////////////////////////////////////////////
			//Open server socket waiting for connection from another client : this blocks thread
			///////////////////////////////////////////////////////////////////////////////////////////
			try (
				ServerSocket chatServerSocket = new ServerSocket(555);
				Socket clientSocket = chatServerSocket.accept();
				PrintWriter chatOut = new PrintWriter(clientSocket.getOutputStream(), true);
				BufferedReader chatIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
					
				// Used to send byte[]
				DataOutputStream dOut = new DataOutputStream(clientSocket.getOutputStream());
				DataInputStream dIn = new DataInputStream(clientSocket.getInputStream());
				)
			{

				// Received a socket connection from the initiating client
				// We are client B, receiving from A
				System.out.println("Request for chat received: Beginning Authentication Process");					
				  
				if (getAuthB(chatIn, chatOut, dIn, dOut))
				{
					//start receive thread that prints all input to the socket from connected client										
					receiveThread rT = new receiveThread(clientSocket, chatIn);					
					rT.start();
					
					System.out.println("Beginning chat:");
					chat(dOut, dIn, chatOut);
				}		
			}
				 catch (IOException e) {
					e.printStackTrace();
				}
		}
		//initiating a connection
		else					
		{
			////////////////////////////////////////////////////////////////////////////
			//make connection to client and wait for Auth server to be contacted 
			///////////////////////////////////////////////////////////////////////////////////////////
			//this opens a connection with the other client
			
			// Opens a connection from us (A) to B.
			try (
				Socket messageSocket = new Socket("localhost", 555);
				PrintWriter out = new PrintWriter(messageSocket.getOutputStream(), true);
				BufferedReader in = new BufferedReader(new InputStreamReader(messageSocket.getInputStream()));
				
				// Used to send byte[]
				DataOutputStream dOut = new DataOutputStream(messageSocket.getOutputStream());
				DataInputStream dIn = new DataInputStream(messageSocket.getInputStream());
			)
			{
				//begin auth process from client A
				if (DEBUG)
					System.out.println("\nBeginning authentication process from A");
				
				if (getAuthA(in, out, dIn, dOut))
				{
					// Auth succeeded
					//start receive thread that prints all input to the socket from connected client
					receiveThread rT1 = new receiveThread(messageSocket, in);				
					rT1.start();
					
					System.out.println("Beginning chat:");
					chat(dOut, dIn, out);
					
				}
				} catch (IOException e) {
					e.printStackTrace();		
				}
		}
		keys.close();
	}
	
	
	/**
	 * Method that controls chat between clients
	 * @param dOut send byte[] to other client
	 * @param dIn receive byte[] from client
	 * @param out send string to client
	 */
	private static void chat(DataOutputStream dOut, DataInputStream dIn, PrintWriter out ) {
		String inputMessage = "";
		while (true)
		{
			System.out.println("\nEnter 'm' to send a message, 'f' to send a file or 'x' to exit:");		
			if (keys.hasNextLine())
			{
				inputMessage = keys.nextLine();
				if (inputMessage.equals("m")) {
					System.out.print("\n>> ");
					byte[] msg = keys.nextLine().getBytes();
					try {
						byte[] encrypted = Crypto.encrypt(msg, sessionKey);	
						
						if (DEBUG) {
							byte[] decrypted = Crypto.decrypt(encrypted, sessionKey);						
							// Debug info
							printInfo(msg, encrypted, decrypted);
							System.out.println("\nThe decrypted message is: ");
							System.out.println(new String(decrypted));
						}
						
						// Tell other client we are sending a regular message
						dOut.writeInt("MESSAGE".length());
						dOut.write("MESSAGE".getBytes());
						
						// send the encrypted byte[]
						dOut.writeInt(encrypted.length);
						dOut.write(encrypted);
					    
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				// file
				else if (inputMessage.equals("f")) {
					System.out.println("Enter the file location and name:");
					File file = new File(keys.nextLine());
					Path path = Paths.get(file.getAbsolutePath());
					
					try {
						// Convert file into a byte array
						byte[] data = Files.readAllBytes(path);	
						
						byte[] encrypted = Crypto.encrypt(data, sessionKey);			
						byte[] decrypted = Crypto.decrypt(encrypted, sessionKey);

						// Debug info
						if (DEBUG) {
							printInfo(data, encrypted, decrypted);	
						}
						
						// Tell it we are sending a file
						dOut.writeInt("FILE".length());
						dOut.write("FILE".getBytes());
						
						dOut.writeInt(encrypted.length);
						dOut.write(encrypted);
						
					} catch (NoSuchFileException e) {
						System.err.println("File not found");
						
					} catch (IOException e) {
						System.err.println("File handling error");
						e.printStackTrace();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else if (inputMessage.equals("x"))
					break;
			}
		}
		System.out.println("Ending Chat.");
		System.exit(0);	
	}

	/**
	 * Performs the authentication process from client B
	 * Handles the request from B to AS
	 * Sets the session key if successful
	 * @param chatIn - input stream from A
	 * @param chatOut - output stream to A
	 * @param dIn - used to receive byte[]
	 * @param dOut - used to send byte[]
	 * @return true if succeeded, else false
	 */
	public static boolean getAuthB(BufferedReader chatIn, PrintWriter chatOut, DataInputStream dIn, DataOutputStream dOut)
	{
		// WE ARE CLIENT B
		try {
			//step 1 - receive request message from client A
			// B <- A: A, nA
			System.out.println("\n1: B <- A: A, nA");
			
			//must decode req to get stuff to send to Auth server
			//req has form "USER_A,NonceA"
			String[] req = chatIn.readLine().split(",");			
			A = req[0];
			nonceA = req[1];
			
			if (DEBUG)
				System.out.println(username + " (B) has received a request from " + A + " (A) [Na: " + nonceA +"]");	
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return false;
		} 
		
		
		//step 2		
		// Make connection to AS
		try (
			Socket authSocket = new Socket("localhost", 444);
			PrintWriter authOut = new PrintWriter(authSocket.getOutputStream(), true);
			BufferedReader authIn = new BufferedReader(new InputStreamReader(authSocket.getInputStream()));
			DataInputStream dInAuth = new DataInputStream(authSocket.getInputStream());
		)
		{
			// Got connection from Auth server 
			
			// Generate a nonce for us.
			nonceB = Crypto.generateNonce();
			B = username;

			// B -> AS: A, B, Na, Nb
			System.out.println("\n2: B -> AS: A, B, Na, Nb");
			authOut.println(A + "," + B + "," + nonceA + "," + nonceB);
			
			//This is where all the message passing happens with the encryption as per diagram
			//send first message to server 	
			Thread.sleep(3000);
			if (true)
			{
				// B <- AS: Eka[Ks, Na, Nb], Ekb[ks, Nb, Na]
				if (DEBUG)
					System.out.println("\n3: B <- AS: Eka[Ks, Na, Nb], Ekb[ks, Nb, Na]");

				//receive auth server return message
				//set session key given by server
				int l = dInAuth.readInt();
				//System.out.println("received LENGTH = " + l );
				byte[] EkaKs = new byte[l];
				//dInAuth.readFully(EkaNa);
				dInAuth.read(EkaKs, 0, EkaKs.length);
				
				byte[] EkaNa = new byte[dInAuth.readInt()];
				//dInAuth.readFully(EkaNa);
				dInAuth.read(EkaNa, 0, EkaNa.length);

				byte[] EkaNb = new byte[dInAuth.readInt()];
				//dInAuth.readFully(EkaNb);
				dInAuth.read(EkaNb, 0, EkaNb.length);

				
				byte[] EkbKs = new byte[dInAuth.readInt()];
				//dInAuth.readFully(EkbKs);
				dInAuth.read(EkbKs, 0, EkbKs.length);

				byte[] EkbNa = new byte[dInAuth.readInt()];
				//dInAuth.readFully(EkbNa);
				dInAuth.read(EkbNa, 0, EkbNa.length);

				byte[] EkbNb = new byte[dInAuth.readInt()];
				//dInAuth.readFully(EkbNb);
				dInAuth.read(EkbNb, 0, EkbNb.length);

				
				//byte[] EksNa = null;
				//byte[] EksNb = null;
				byte[] EksNaNb = null;
				
				//System.out.println("SIZE = " + EkbKs.length);
				//System.out.println("Key " + authKey);
				try {
					sessionKey = new String(Crypto.decrypt(EkbKs, authKey));
					System.out.println("Session key received");

					receivedNonceA = new String(Crypto.decrypt(EkbNa, authKey));
					receivedNonceB = new String(Crypto.decrypt(EkbNb, authKey));
					
					String NaNb = receivedNonceA + "," + nonceB;
					
					// Encrypt Na and Nb with the session key to prove to A we have the key
					//EksNa = Crypto.encrypt(receivedNonceA.getBytes(), sessionKey);
					//EksNb = Crypto.encrypt(nonceB.getBytes(), sessionKey);
					EksNaNb = Crypto.encrypt(NaNb.getBytes(), sessionKey);
							

				} catch (Exception e) {
					e.printStackTrace();
				}
				if (DEBUG) {
					System.out.println(username + " (B) has received the encrypted data from the AS");
					System.out.println("Ks = " + sessionKey);
				}
				
				// Check that we received the correct nonce back
				if (receivedNonceB.equals(nonceB))
					System.out.println("The encrypted nonce[B] received from the server matches our nonce.");
				else {
					System.err.println("The encrypted nonce[B] received from the server does not match our nonce.");
					return false;
				}
				//step 3: send session key back to client A
				// B -> A : Eka[Ks, Na], Eks[Na, Nb]		
				if (DEBUG)
					System.out.println("\n4: B -> A : Eka[Ks, Na], Eks[Na, Nb]");
				dOut.writeInt(EkaKs.length);
				dOut.write(EkaKs);
				dOut.writeInt(EkaNa.length);
				dOut.write(EkaNa);
				
				dOut.writeInt(EksNaNb.length);
				dOut.write(EksNaNb);
				//dOut.writeInt(EksNa.length);
				//dOut.write(EksNa);
				
				// step 5: finally verify that A is actually A and has session key
				// B <- A: Eks[Nb]
				if (DEBUG)
					System.out.println("\n5: B <- A: Eks[Nb]");
				byte[] EksNb = new byte[dIn.readInt()];
				dIn.readFully(EksNb);
				
				receivedNonceB = new String(Crypto.decrypt(EksNb, sessionKey));
				// Check that we received the correct nonce back from A (encrypted with session key
				if (receivedNonceB.equals(nonceB))
					System.out.println("The encrypted nonce[B] received from A (encrypted with Ks) matches our nonce.");
				else {
					System.err.println("The encrypted nonce[B] received from A (encrypted with Ks) does not match our nonce.");
					return false;
				}

				System.out.println("\nInitialisation complete\n");
				return true ; 				
				
			}			
			} catch (IOException e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return false;
	}

	/**
	 * Performs the authentication process from client A
	 * @param chatIn - input stream from B
	 * @param chatOut - output stream to B
	 * @param dIn - used to receive byte[]
	 * @param dOut - used to send byte[]
	 * @return true if succeeded, else false
	 */
	public static boolean getAuthA(BufferedReader chatIn, PrintWriter chatOut, DataInputStream dIn, DataOutputStream dOut)
	{
		// WE ARE CLIENT A

		//Step 1
		/////////////////////////////////////////////////////////////////////
		// A -> B: A, nA
		System.out.println("\n1: A -> B: A, nA");

		nonceA = Crypto.generateNonce();
		
		//for checking B has same session key (B must return nonce A encrypted with session key)
		String sessionKeyNonceA = "";
		
		//send first message
		chatOut.println(username + "," + nonceA);
		
		//Step 2
		/////////////////////////////////////////////////////////////////////
		// A <- B : Eka[Ks, Na], Eks[Na, Nb]
		try {
			System.out.println("\n4: A <- B : Eka[Ks, Na], Eks[Na, Nb]");
			byte[] EkaKs = new byte[dIn.readInt()];
			dIn.readFully(EkaKs);
			byte[] EkaNa = new byte[dIn.readInt()];
			dIn.readFully(EkaNa);
			
			byte[] EksNaNb = new byte[dIn.readInt()];
			dIn.readFully(EksNaNb);
			//byte[] EksNb = new byte[dIn.readInt()];
			//dIn.readFully(EksNb);

			sessionKey = new String(Crypto.decrypt(EkaKs, authKey));
			receivedNonceA = new String(Crypto.decrypt(EkaNa, authKey));
			
			String[] nonces = (new String(Crypto.decrypt(EksNaNb, sessionKey))).split(",");
			receivedNonceB = nonces[1];
			sessionKeyNonceA = nonces[0];
			
			// For later verifying to B we are A and have the session key
			byte[] EksNb = Crypto.encrypt(receivedNonceB.getBytes(), sessionKey);
			
			if (DEBUG) {
				System.out.println(username + " (A) has received the encrypted data from B");		
				System.out.println("Ks = " + sessionKey);		
				System.out.println("Nb = " + receivedNonceB);
				
				// Check that we received the correct nonce back 
				if (receivedNonceA.equals(nonceA))
					System.out.println("The AS encrypted nonce[A] received from the server matches our nonce.");
				else {
					System.err.println("The AS encrypted nonce[A] received from the server does not match our nonce.");
					return false;
				}
			}

			// Verify to B that we have the session key
			// A -> B: Eks[Nb]
			System.out.println("\n5:  A -> B: Eks[Nb]");

			dOut.writeInt(EksNb.length);
			dOut.write(EksNb);
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		


		
		// Check that B has got the same session key
		if (nonceA.equals(sessionKeyNonceA))
			System.out.println("The session key encrypted nonce[A] received from B matches our nonce.");
		else {
			System.err.println("The session key encrypted nonce[A] received from B does not match our nonce.");
			return false;
		}
		

		System.out.println("\nInitialisation complete\n");
		return true;
	}
	
	
	//this tread does all the receiving for each client. Chat Decrypt happens in here
	public static class receiveThread extends Thread 
	{
		private Socket ChatSocket = null ; 
		private DataInputStream is = null ;
		private BufferedReader in = null;
		
		
	public receiveThread (Socket chSocket, BufferedReader in2)
	{
		this.ChatSocket = chSocket;
		this.in  = in2;
	}
	
	
	@SuppressWarnings("deprecation")
	public void run()
	{
		
		DataInputStream dIn = null;
		try {
			//System.out.println("Creating read thread scanner");
		//	 s = new Scanner(ChatSocket.getInputStream());
			 dIn = new DataInputStream(ChatSocket.getInputStream());
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		
		while (true)
		{
			byte[] encrypted;	
			try {
				// See if message or file
				byte[] type = new byte[dIn.readInt()];
				dIn.readFully(type);
				
				// get encrypted data
				encrypted = new byte[dIn.readInt()];
				dIn.readFully(encrypted);

				byte[] decrypted = Crypto.decrypt(encrypted, sessionKey);
			
				if (new String (type).equals("MESSAGE")) {
					System.out.println("\nEncrypted message received:");
					System.out.println(">" + new String(decrypted) +"\n");
					
					if (DEBUG) {
						System.out.println("Encrypted: " + Arrays.toString(encrypted));
						System.out.println("Decrypted: " + Arrays.toString(decrypted));
					}
				}
				else // file 
					System.out.println("Encrypted File received");			
				
				//save file
				
				try {
					
					System.out.println("Enter file name to save the file:");
					Scanner s = new Scanner(System.in);
					
					
					String name = s.nextLine();
								
					while (name.length() < 4)
					{
						name = s.nextLine();
					}
					
					
					s.close();
					
					System.out.println("Saving file to " + System.getProperty("java.class.path")+"/"+name);
					FileOutputStream fos = new FileOutputStream(System.getProperty("java.class.path")+"/"+name);
					
					fos.write(decrypted);
					
					fos.close();
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
						System.out.println("File saved at "+ System.getProperty("java.class.path"));
					
			} catch (IOException e) {
				System.out.println("Connection terminated");
				System.exit(0);
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			
		}
	
	}
	
	}

	/* prints out some debug info */
	private static void printInfo(byte[] original, byte[] encrypted, byte[] decrypted) {
		System.out.println("Original: " + Arrays.toString(original));
		System.out.println("Cipher  : " + Arrays.toString(encrypted));
		System.out.println("Decrypt : " + Arrays.toString(decrypted));
		
		System.out.println("\nChecking that the decrypted data equals the original data...");
		if (!Arrays.equals(original, decrypted)) 
			System.err.println("Data was not decrypted successfully");
		else 
			System.out.println("The original data and the decrypted data are the same");


	}
	
}

	
	