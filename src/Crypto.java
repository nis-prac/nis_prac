import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Performs encryption and decryption using AES-CBC mode.
 * @author Craig Feldman
 * 18 March 2015
 */
public class Crypto {
	private final static boolean DEBUG = !true;
	// Cryptographically secure RNG
	private static SecureRandom random = new SecureRandom();	

	// Prevents instantiation of this class effectively treating it as a static class
	private Crypto() {};
	
	/**
	 * Encrypts the input using the specified key and AES-CBC encryption.
	 * @param input a byte array containing the data to be encrypted.
	 * @param key a 128 bit key used for encryption.
	 * @return a byte array containing the encrypted data. The first block is used to store the plain text IV.
	 * @throws Exception 
	 */
	public static byte[] encrypt(byte[] input, String key) throws Exception {
		// Get a cipher instance based on the specified crypto algorithm
		// Uses Advanced encryption standard, Cipher Block Chaining (requires IV) and pads to required size
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		
		//construct a secret key form the given byte array
		SecretKeySpec k = new SecretKeySpec(key.getBytes(), "AES");
		
		// Generate a random IV
		IvParameterSpec iv = cipher.getParameters().getParameterSpec(IvParameterSpec.class);
		
		//initialise the cipher for encryption with the specified key and IV
		cipher.init(Cipher.ENCRYPT_MODE, k, iv);

		
		byte[] ivArr = iv.getIV();
		byte[] encryptedArr = cipher.doFinal(input);

		if (DEBUG)
			System.out.println("\nIV (E): " + Arrays.toString(ivArr));
		//	System.out.println("Encrypted: " + Arrays.toString(encryptedArr));

		// Prepend IV to cipher 
		byte[] combinedArr = new byte[ivArr.length + encryptedArr.length];
		System.arraycopy(ivArr, 0, combinedArr, 0, ivArr.length);
		System.arraycopy(encryptedArr, 0, combinedArr, 16, encryptedArr.length);
		
		//System.out.println("Final Combined: " + Arrays.toString(combinedArr) + "\n");
		
		//Perform the encryption and return the result
		return combinedArr;
	}
	
	/**
	 * Decrypts the input using AES-CBC decryption.
	 * Assumes the first 16 bytes of the input are the IV.
	 * @param input - The data to decrypt (byte[0..16] = IV).
	 * @param key - the key to be used for decryption.
	 * @return - a byte array containing the decrypted data
	 * @throws Exception
	 */
	public static byte[] decrypt(byte[] input, String key) throws Exception {
		// First 16 bytes are the IV
		byte[] encryptedArr = Arrays.copyOfRange(input, 16, input.length);
		
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");		
		SecretKeySpec k = new SecretKeySpec(key.getBytes(), "AES");
		IvParameterSpec iv = new IvParameterSpec(input, 0, 16);
		
		if (DEBUG) 
			System.out.println("IV (D): " + Arrays.toString(iv.getIV()) + "\n");

		// Initialise with the specified key and the extracted IV
		cipher.init(Cipher.DECRYPT_MODE, k, new IvParameterSpec(input, 0, 16));
		
		//Perform the decryption and return the result		
		return cipher.doFinal(encryptedArr);
	}
	
	/** Generates and returns a random nonce */
	public static String generateNonce(){
		 return new BigInteger(130, random).toString(32);
	}
	
	/** Generates and returns a random, cryptographically secure key */
	public static String generateKey() {
		 return new BigInteger(130, random).toString(32).substring(0, 16);
	}
		
	/** 
	 * Converts the byte array into readable text. Provided the original data was an ASCII message.
	 * @param data the byte array to convert.
	 * @return A string containing the message.
	 */
	public static String convertToString(byte[] data) {
		return new String(data);
	}
	


}
