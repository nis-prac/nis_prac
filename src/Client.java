import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Scanner;

/**
 * This is the client side class
 * @author Craig Feldman
 * 18 March 2015
 */
public class Client {
	
	final static boolean DEBUG = true;
	
	static String encryptionKey;
	static Crypto crypto;
	static Scanner keyboard;
	
	public Client(){
		//TODO get and set encryption key
		encryptionKey = "0123456789abcdef";
		crypto = new Crypto(encryptionKey);
	}
	
	public static void main(String[] args) {
		System.out.println("===============================");
		System.out.println("     Welcome to Secure Chat    ");	
		System.out.println("===============================");
		System.out.println("Please enter an adress to connect to or wait for a connection...\n");		
		
		//TODO - Replace with call to new Client?
		encryptionKey = "0123456789abcdef";
		crypto = new Crypto(encryptionKey);
		
		// Print the menu
		keyboard = new Scanner(System.in);
		System.out.println("Enter 'm' to send a message, 'f' to send a file or 'x' to exit:");		
		while (keyboard.hasNextLine()) {
			String input = keyboard.nextLine();
			if (input.equals("m"))
				sendMessage();
			else if ((input.equals("f")))
				sendFile();
			else if ((input.equals("x"))) {
				System.out.println("Good Bye");
				System.exit(0);
			}
			else {
				System.out.println("Invalid Command");
			}
			System.out.println("\nEnter 'm' to send a message, 'f' to send a file or 'x' to exit:");

		}
	}
	

	
	/* Sends encrypted file from A to B */
	private static void sendFile() {
		System.out.println("Enter the file location and name:");
		//Scanner keyboard = new Scanner(System.in);
		File file = new File(keyboard.nextLine());
		Path path = Paths.get(file.getAbsolutePath());
		
		try {
			// Convert file into a byte array
			byte[] data = Files.readAllBytes(path);	
			
			byte[] encrypted = crypto.encrypt(data, crypto.getKey());			
			byte[] decrypted = crypto.decrypt(encrypted, crypto.getKey());

			// Debug info
			printInfo(data, encrypted, decrypted);	
			
		} catch (NoSuchFileException e) {
			System.err.println("FIle not found");
			sendFile();
		} catch (IOException e) {
			System.err.println("File handling error");
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		//keyboard.close();

	}

	/* Sends encrypted message from A to B */
	private static void sendMessage() {
		System.out.print("\n>> ");
		//Scanner keyboard = new Scanner(System.in);
		byte[] msg = keyboard.nextLine().getBytes();
		//keyboard.close();
		
		try {
			byte[] encrypted = crypto.encrypt(msg, crypto.getKey());	
			
			if (DEBUG) {
				byte[] decrypted = crypto.decrypt(encrypted, crypto.getKey());
			
				// Debug info
				printInfo(msg, encrypted, decrypted);
				System.out.println("\nThe decrypted message is: ");
				System.out.println(crypto.convertToString(decrypted));
			}
		    
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private static void printInfo(byte[] original, byte[] encrypted, byte[] decrypted) {
		System.out.println("Original: " + Arrays.toString(original));
		System.out.println("Cipher  : " + Arrays.toString(encrypted));
		System.out.println("Decrypt : " + Arrays.toString(decrypted));
		
		System.out.println("\nChecking that the decrypted data equals the original data...");
		if (!Arrays.equals(original, decrypted)) 
			System.err.println("Data was not decrypted successfully");
		else 
			System.out.println("The original data and the decrypted data are the same");


	}
	
}
